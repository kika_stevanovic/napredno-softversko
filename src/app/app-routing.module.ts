import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ChartComponent } from './chart/chart.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { AreaChartComponent} from './area-chart/area-chart.component'

const routes: Routes = [
  {path: 'line-chart', component: ChartComponent},
  {path: 'bar-chart', component: BarChartComponent},
  {path: 'area-chart', component: AreaChartComponent},
  {path: 'configuration', component: ConfigurationComponent},
  {path: '**', component: ChartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
