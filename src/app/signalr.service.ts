import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {SensorData} from './sensor-data';
import {ReportLevel} from './configuration/configuration.component';


declare var $: any;

@Injectable({
  providedIn: 'root'
})


export class SignalrService {

  private connection: any;
  private proxy: any;
  sensorData: Subject<SensorData>=new Subject();
  avgData: Subject<SensorData>=new Subject();

  constructor() { }
  
  public initializeSignalRConnection(): void {
    let signalRServerEndPoint = 'http://localhost:49568';
          this.connection = $.hubConnection(signalRServerEndPoint);
          this.proxy = this.connection.createHubProxy('sensorHub');
    
    this.proxy.on('sendSensorData', (timestamp,temperature,humidity) => {

      this.sensorData.next(new SensorData(timestamp, temperature, humidity));
      console.log('New message received from Server: ' + timestamp+' '+temperature+ ' '+humidity );
        
    });

    this.proxy.on('sendAvgSensorData', (timestamp,temperature,humidity) => {

      this.avgData.next(new SensorData(timestamp,temperature,humidity));
      console.log('New message received from Server: ' + timestamp+' '+temperature+ ' '+humidity);
      
    });

  
    this.connection.start().done((data: any) => {
              console.log('Connected to Notification Hub');
              
          }).catch((error: any) => {
              console.log('Notification Hub error -> ' + error);
          });
      }
 
}
