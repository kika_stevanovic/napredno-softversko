export class SensorData {
   temperature:number;
    timestamp:string;
    humidity:number;
    constructor(timestamp:string,temperature:number,humidity:number){
        this.timestamp=timestamp;
        this.humidity=humidity;
        this.temperature=temperature;
    }
}
