import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ReportLevel} from './configuration/configuration.component';
import {Observable} from 'rxjs'


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  updateReportLevel(reportModel: ReportLevel): any {
   this.http.put("http://localhost:49568/api/ReportLevels/" +"/"+ reportModel.id, reportModel)
   .subscribe(data => {console.log("data")});
  
  }

  deleteReportLevel(id: number): any {
    console.log("delete",id);
   return this.http.delete("http://localhost:49568/api/ReportLevels/"+id);
  }

  

  constructor(private http: HttpClient) { }
  
  getAvgSensorData(): Observable<any> {
    return this.http.get("http://localhost:49568/api/SensorData");
} 

getReportLevels(): Observable<any> {
  return this.http.get("http://localhost:49568/api/ReportLevels");
} 

/*
postReportLevel(reportLevel: ReportLevel): Observable<ReportLevel> {
  console.log("postReportLevel");
  return this.http.post<ReportLevel>("http://localhost:49568/api/ReportLevels", reportLevel);
} 
*/
 postReportLevel(reportLevel: ReportLevel):Observable<any>
  {
  console.log("HTTP req");
 return this.http.post("http://localhost:49568/api/ReportLevels",reportLevel
  );
 
  
}
getAllSensorData(all : number): Observable<any> {
  console.log("get all");
  return this.http.get("http://localhost:49568/api/SensorData/" +all);
  
}
  
}
