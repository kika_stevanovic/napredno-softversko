import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { HttpService } from 'src/app/http.service';
import { SensorData } from '../sensor-data';
import { SignalrService } from 'src/app/signalr.service';


@Component({
  selector: 'app-barchart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

  chart: Chart;
  counter=0;
  data=[2,4,5,6];
 
  constructor(public signalRService: SignalrService, public httpService: HttpService) { }

  getAvgData() {
    this.httpService.getAvgSensorData().subscribe(
       data => { 
          
    console.log(data);
    for (let index = 0; index < data.length; index++) {
      

      const element = data[index] as SensorData;
      this.chart.addPoint([(new Date(element.timestamp).getTime()),element.temperature],0,true,false);
      this.chart.addPoint([(new Date(element.timestamp).getTime()),element.humidity],1,true,false);
     
      
    }
   
    
	 }
       );	   
} 
  ngOnInit() {
  
    
    this.init();
    this.getAvgData();

    this.signalRService.avgData.subscribe(data=>{

      console.log(data);
      this.chart.removePoint(5,0);
      this.chart.removePoint(5,1);
      console.log("removed");
      this.chart.addPoint([(new Date(data.timestamp).getTime()),data.temperature],0,true,false);
      this.chart.addPoint([(new Date(data.timestamp).getTime()),data.humidity],1,true,false);
      console.log("added");
     
      
    })
    
    
    

    }
  
  init() {
    let chart = new Chart({
      chart: {
        renderTo: 'container',
        defaultSeriesType: 'column',
        
      },
    
      title: {
        text: 'Average Temperature and Humidity'
      },
      subtitle: {
        text: 'Source:  senzor'
      },
      credits: {
        enabled: true
      },
      xAxis: {
        type: 'datetime',
        tickPixelInterval: 200,
        maxZoom: 20 * 1000
      },
      yAxis: {
        //minPadding: 0.2,
        //maxPadding: 0.2,
        title: {
          text: 'Value',
          margin: 80
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      series: [{
        name: 'Temperature',
        data: []
      },
      {
        name: 'Humidity',
        data: []
      }]
    });
    
    this.chart = chart;
    
    
    //chart.ref$.subscribe(console.log);
  }
  

}