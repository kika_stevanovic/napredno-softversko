import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { SignalrService } from 'src/app/signalr.service';
import {SensorData} from 'src/app/sensor-data'
import { ReportLevel } from '../configuration/configuration.component';
import { HttpService } from 'src/app/http.service';



@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
})
export class ChartComponent implements OnInit {
  mainChart: Chart;
  temperatureChart: Chart;
  humidityChart: Chart;
  reportLevels: ReportLevel[];
  thiss: any;
 
  counter=0;
  constructor(public signalRService: SignalrService,public http: HttpService) { }

  ngOnInit() {
    this.thiss = this;
    this.InitializeTempChart();
    this.InitializeHumChart();

    let zones: any;
    this.reportLevels = [];
    zones = this.CreateChartZones();

    this.InitializeMainChart(zones);

    this.signalRService.initializeSignalRConnection();   
   
    this.signalRService.sensorData.subscribe(data=>{
      this.AddPointsToMainChart(data);
      this.AddPointToChart(data.timestamp, data.temperature, this.temperatureChart);
      this.AddPointToChart(data.timestamp, data.humidity, this.humidityChart); 
    })
    
  }

  CreateChartZones()
  {
    let zones: any;
         
        this.http.getReportLevels().subscribe(
          data => { 
         
          let tempZones: any[] = [];
          let humZones: any[] = [];
         
          
          console.log(data);
          for (let index = 0; index < data.length; index++) {
            const reportLvl = data[index] as ReportLevel;
            this.reportLevels.push(reportLvl);

            if(reportLvl.type == "temperature")
            {
              tempZones.push({
                value: reportLvl.max,
                color: reportLvl.color
              });
            }
            else
            {
              console.log(reportLvl.color + " " + reportLvl.max);
              humZones.push({
                value: reportLvl.max,
                color: reportLvl.color
              });
            }
           
          }
          

          this.humidityChart.ref.update({
            plotOptions: {
              series: {
                zoneAxis: 'y',
                zones: humZones
              }
          }
        });


        this.temperatureChart.ref.update({
          plotOptions: {
            series: {
                zoneAxis: 'y',
                zones: tempZones
              
            }
        }
      });

      zones = {
        temperatureZone: tempZones,
        humidityZone: humZones
      };

      });

    
      return zones;
  }

  AddPointToChart(timestamp: string, data:number, chart: Chart) {
    if (chart) {
      var shift = this.counter > 20;

      chart.addPoint([ new Date(timestamp).getTime() , data],0, true, shift);
      
      this.counter++;
     
    } 
  }

  AddPointsToMainChart(data:SensorData) {
    if (this.mainChart) {
      var shift = this.counter > 20;

      this.mainChart.addPoint([ new Date(data.timestamp).getTime() ,data.temperature],0, true, shift);
      this.mainChart.addPoint([ new Date(data.timestamp).getTime(),data.humidity],1, true, shift);
      
     this.counter++;
     
    }
  }

  getZoneName(y:any)
  {
  for (let index = 0; index < this.reportLevels.length; index++) {
              const reportLvl = this.reportLevels[index] as ReportLevel;
              if(y >= reportLvl.min && y <= reportLvl.max)
              {
                return reportLvl;
              }
  }
              
  }


  InitializeHumChart() {
    let chart = new Chart({
      chart: {
        renderTo: 'container',
       
        defaultSeriesType: 'spline',
      },
      title: {
        text: 'Humidity chart'
      },
      credits: {
        enabled: true
      },
      xAxis: {
        type: 'datetime',
        tickPixelInterval: 150,
        maxZoom: 20 * 1000
      },
      yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
          text: 'Value',
          margin: 80
        }
      },
      tooltip: {
        valueSuffix: '%',
    /*    
        formatter: function()
        {
          var reportLevel = this.getZoneName(this.y);

          return  'The value for <b>' + reportLevel.type +
          '</b> is <b>' + this.y + '</b>' + "; Zone level :" + reportLevel.name;
        }
        */
      },
      series:
       [{
        name: 'Humidity',
        color: '#f99c31',
        data: []
      }]
    });
   
    this.humidityChart = chart;
  

    chart.ref$.subscribe(console.log);
  }

  InitializeTempChart() {
    let chart = new Chart({
      chart: {
        renderTo: 'container',
       
        defaultSeriesType: 'spline',
      },
      title: {
        text: 'Temperature chart'
      },
      credits: {
        enabled: false
      },
      xAxis: {
        type: 'datetime',
        tickPixelInterval: 150,
        maxZoom: 20 * 1000
      },
      yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
          text: 'Value',
          margin: 80
        }
      },
      tooltip: {
        valueSuffix: '°C'
      },
      series:
       [{
        name: 'Temperature',
        data: []
      }]
    });
   
    this.temperatureChart = chart;
  

    chart.ref$.subscribe(console.log);
  }

  InitializeMainChart(zonesAny: any) {
    let chart = new Chart({
      chart: {
        renderTo: 'container',
       
        defaultSeriesType: 'spline',
      },
      title: {
        text: 'Temperature and Humidity chart'
      },
      credits: {
        enabled: true
      },
      xAxis: {
        type: 'datetime',
        tickPixelInterval: 150,
        maxZoom: 20 * 1000
      },
      yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
          text: 'Value',
          margin: 80
        }
      },
      series:
       [{
        name: 'Temperature',
        data: []
         
      },
      {
        name: 'Humidity',
        color: '#f99c31',
        data: []
      }]
    });
   
    this.mainChart = chart;
  

    chart.ref$.subscribe(console.log);
  }



}
