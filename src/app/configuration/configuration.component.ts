import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/http.service';

export class ReportLevel {


  public min: number;
  public max: number;
  public id:  number;


  constructor(
    public name: string = '',
    public type: string = 'Select type',
    public color: string = 'Select color',
    public notification:  string = "No",
    public notificationSent:  boolean = false,
    public email:  string = ""
  ) {
    
  }
}

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})


  export class ConfigurationComponent implements OnInit {
    // It maintains list of Registrations
    reportLevels: ReportLevel[] = [];
    // It maintains registration Model
    reportModel: ReportLevel;
    // It maintains registration form display status. By default it will be false.
    showNew: Boolean = false;
    // It will be either 'Save' or 'Update' based on operation.
    submitType: string = 'Save';
    // It maintains table row index based on selection.
    selectedRow: number;
    // It maintains Array of countries.
    colors: string[] = ['red', 'yellow', 'green', 'blue', 'orange', 'pink', 'purple'];
    types: string[] =['temperature', 'humidity'];
    decision: string[] =['yes', 'no'];
    constructor(public http: HttpService) {
     
    }
  
    ngOnInit() {

      this.http.getReportLevels().subscribe(
        data => { 
           
     console.log(data);
     for (let index = 0; index < data.length; index++) {
       
 
       const reportLvl = data[index] as ReportLevel;
       this.reportLevels.push(reportLvl);
       
     }
 
    }
        );	
    }
  
    // This method associate to New Button.
    onNew() {
      // Initiate new registration.
      this.reportModel = new ReportLevel();
      // Change submitType to 'Save'.
      this.submitType = 'Save';
      // display registration entry section.
      this.showNew = true;
    }
  
    // This method associate to Save Button.
    onSave() {
     
      if (this.submitType === 'Save') {
       
        // Push registration model object into registration list.
        
        this.http.postReportLevel(this.reportModel).subscribe(
          data => {
              console.log("POST Request is successful ", data);
              this.reportModel.id=data;
              this.reportLevels.push(this.reportModel);
          },
          error => {
              console.log("Error", error);
          }
      );   ;
        

      } else {
      
        this.http.updateReportLevel(this.reportModel);
        // Update the existing properties values based on model.
        this.reportLevels[this.selectedRow].name = this.reportModel.name;
        this.reportLevels[this.selectedRow].type = this.reportModel.type;
        this.reportLevels[this.selectedRow].max = this.reportModel.max;
        this.reportLevels[this.selectedRow].min = this.reportModel.min;
        this.reportLevels[this.selectedRow].color = this.reportModel.color;
        this.reportLevels[this.selectedRow].notification = this.reportModel.notification;
        this.reportLevels[this.selectedRow].notificationSent = this.reportModel.notificationSent;
        this.reportLevels[this.selectedRow].email = this.reportModel.email;

      }
      // Hide registration entry section.
      this.showNew = false;
    }
  
    // This method associate to Edit Button.
    onEdit(index: number) {
      // Assign selected table row index.
      this.selectedRow = index;
      // Initiate new registration.
      this.reportModel = new ReportLevel();
      // Retrieve selected registration from list and assign to model.
      this.reportModel = Object.assign({}, this.reportLevels[this.selectedRow]);
      // Change submitType to Update.
      this.submitType = 'Update';
      // Display registration entry section.
      this.showNew = true;
    }
  
    // This method associate to Delete Button.
    onDelete(index: number) {
      // Delete the corresponding registration entry from the list.
      this.http.deleteReportLevel(this.reportLevels[index].id).subscribe( data => {
        console.log("Deleted ", data);
       
        this.reportLevels.splice(index, 1);
    },
    error => {
        console.log("Error", error);
    });
      
    }
  
    // This method associate toCancel Button.
    onCancel() {
      // Hide registration entry section.
      this.showNew = false;
    }
  
    // This method associate to Bootstrap dropdown selection change.
    onChangeColor(color: string) {
      // Assign corresponding selected country to model.
      this.reportModel.color = color;
    }

    onChangeType(type: string) {
      // Assign corresponding selected country to model.
      this.reportModel.type = type;
    }

    onChangeDecision(d: string) {
      // Assign corresponding selected country to model.
      this.reportModel.notification = d;

    }
  
  }
