import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { HttpService } from 'src/app/http.service';
import { SignalrService } from 'src/app/signalr.service';
import { SensorData } from '../sensor-data';

@Component({
  selector: 'app-area-chart',
  templateUrl: './area-chart.component.html',
  styleUrls: ['./area-chart.component.css']
})
export class AreaChartComponent implements OnInit {
  temperatureAreaChart: Chart;
  humidityAreaChart: Chart;

  constructor(public signalRService: SignalrService, public httpService: HttpService) { }

  ngOnInit() {
     this.signalRService.initializeSignalRConnection(); 
    this.InitializeAreaTempChart();
    this.InitializeAreaHumChart();

    this.httpService.getAvgSensorData().subscribe(data=>{
      //show data
      console.log("subscribe" + data)
      for (let index = 0; index < data.length; index++) {
      console.log("for loop" + index + " ");

        const element = data[index] as SensorData;
        this.temperatureAreaChart.addPoint([(new Date(element.timestamp).getTime()),element.temperature],0,true,false);
        this.humidityAreaChart.addPoint([(new Date(element.timestamp).getTime()),element.humidity],0,true,false);

      }
      //set listener on new data
     this.setListenerForNewData();
    });
  
  }
  setListenerForNewData(): any {
    this.signalRService.avgData.subscribe(data=>{

      console.log(data);
      this.humidityAreaChart.removePoint(4,0);
      this.temperatureAreaChart.removePoint(4,0);
      console.log("removed");
      this.temperatureAreaChart.addPoint([(new Date(data.timestamp).getTime()),data.temperature],0,true,false);
      this.humidityAreaChart.addPoint([(new Date(data.timestamp).getTime()),data.humidity],0,true,false);
      console.log("added");
      
    })
  }
  InitializeAreaHumChart(): any {
    let chart = new Chart({
      chart: {
        renderTo: 'container',
       
        defaultSeriesType: 'areaspline',
      },
      title: {
        text: 'Humidity chart'
      },
      credits: {
        enabled: true
      },
      xAxis: {
        type: 'datetime',
        tickPixelInterval: 150,
        maxZoom: 20 * 1000
      },
      yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
          text: 'Value',
          margin: 80
        }
      },
      tooltip: {
        valueSuffix: '%'
      }
     
    ,
      series:
       [{
        name: 'Humidity',
        color: '#f99c31',
        data: []
      }]
    });
   
    this.humidityAreaChart = chart;
  

   // chart.ref$.subscribe(console.log);
    
  }
  InitializeAreaTempChart(): any {
    let chart = new Chart({
      chart: {
        renderTo: 'container',
       
        defaultSeriesType: 'areaspline',
      },
      title: {
        text: 'Temperature chart'
      },
      credits: {
        enabled: true
      },
      xAxis: {
        type: 'datetime',
        tickPixelInterval: 150,
        maxZoom: 20 * 1000
      },
      yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
          text: 'Value',
          margin: 80
        }
      },
      tooltip: {
      
    
        valueSuffix: '°C',
        
      },
      series:
       [{
        name: 'Temperature',
        data: []
      }]
    });
   
    this.temperatureAreaChart = chart;
  

    //chart.ref$.subscribe(console.log);
  }

}
